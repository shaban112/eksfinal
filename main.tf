# Configure AWS Provider
provider "aws" {
  region = var.region # Update with your desired region
}


module "eks" {
  source = "./modules/eks" 
 
}

