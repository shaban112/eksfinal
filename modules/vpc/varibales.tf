variable "vpc_cidr_block" {
  description = "CIDR block for the VPC"
  default     = "10.0.0.0/16"
}

variable "availability_zones" {
  description = "List of availability zones"
  default     = ["us-east-1a", "us-east-1b" , "us-east-1c"]  # Adjust with your desired AZs
}
variable "subnet_cidr_blocks_pub" {
  description = "CIDR blocks for the subnets"
  default     = ["10.0.1.0/24", "10.0.2.0/24" ,"10.0.3.0/24"]
}

variable "subnet_cidr_blocks_prv" {
  description = "CIDR blocks for the subnets"
  default     = ["10.0.4.0/24", "10.0.5.0/24" , "10.0.6.0/24"]
}

variable "subnet_names_pub" {
  description = "Names for the subnets"
  default     = ["pub-subnet-1", "pub-subnet-2" , "pub-subnet-3"]
}

variable "subnet_names_prv" {
  description = "Names for the subnets"
  default     = ["prv-subnet-1", "prv-subnet-2" , "prv-subnet-3"]
}
 variable "pre" {
  type           = string
  description  = "prefix"
  default       = "eks"
}


