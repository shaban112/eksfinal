output "subnet_pub" {
  value = aws_subnet.public_subnet
}

output "subnet_prv" {
  value = aws_subnet.private_subnet
}
