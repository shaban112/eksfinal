# VPC Resources
resource "aws_vpc" "eks_vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "${var.pre}-vpc"
  }
}


resource "aws_eip" "eip" {
 domain = "vpc"
 tags = {
    Name = "${var.pre}-eip"
 }
}
resource "aws_internet_gateway" "eks_gw" {
 vpc_id = aws_vpc.eks_vpc.id 
 tags = {
    Name = "${var.pre}-gw"
 }
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public_subnet[0].id
  tags = {
    Name = "${var.pre}-natgw"
 }
}

resource "aws_subnet" "public_subnet" {
 count = length(var.subnet_cidr_blocks_pub)
 vpc_id = aws_vpc.eks_vpc.id
 cidr_block = var.subnet_cidr_blocks_pub[count.index]
 availability_zone = var.availability_zones[count.index]
 tags = {
   Name = var.subnet_names_pub[count.index]
 }
 
}

resource "aws_subnet" "private_subnet" {
count = length(var.subnet_cidr_blocks_prv)
vpc_id = aws_vpc.eks_vpc.id
cidr_block = var.subnet_cidr_blocks_prv[count.index]
availability_zone = var.availability_zones[count.index]
 tags = {
   Name = var.subnet_names_prv[count.index]
 }
  }

resource "aws_default_route_table" "public_route" {
  default_route_table_id = aws_vpc.eks_vpc.default_route_table_id
  route  {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.eks_gw.id
  }
  tags = {
    Name = "${var.pre}-public-route-table"
  }
}

resource "aws_route_table" "private_route" {
  vpc_id = aws_vpc.eks_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-gw.id
  }
  tags = {
    Name = "${var.pre}-private-route-table"
  }
}
resource "aws_route_table_association" "public_assco" {
  count = length(aws_subnet.public_subnet)
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id  = aws_default_route_table.public_route.id
}

resource "aws_route_table_association" "private_assco" {
  count = length(aws_subnet.private_subnet)
  subnet_id      = aws_subnet.private_subnet[count.index].id
  route_table_id  = aws_route_table.private_route.id
}

