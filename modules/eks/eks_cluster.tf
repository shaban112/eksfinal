resource "aws_eks_cluster" "cluster" {
  name     = var.cluster-name
  role_arn = aws_iam_role.eks_role_cluster.arn  
  vpc_config {
    subnet_ids = module.vpc.subnet_pub[*].id
    endpoint_private_access = true
    endpoint_public_access =  true
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.prod_attach_policy_cluster,
  ]
}

data "aws_iam_policy_document" "assume_role_cluster" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "eks_role_cluster" {
  name  = "eks-cluster-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_cluster.json
}

resource "aws_iam_role_policy_attachment" "prod_attach_policy_cluster" {
   count = length(var.attach_policy_cluster)
   policy_arn = var.attach_policy_cluster[count.index]
   role = aws_iam_role.eks_role_cluster.name
 }

resource "aws_eks_addon" "addon" {
  count = length(var.addon_names)
  cluster_name = aws_eks_cluster.cluster.name
  addon_name   = var.addon_names[count.index]
}

resource "aws_iam_openid_connect_provider" "oidc" {
  url = "https://oidc.eks.${var.region}.amazonaws.com/id/${aws_eks_cluster.cluster.name}"
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [var.oidc-thump]  # Update with correct thumbprint

  tags = {
    Name = "eks-cluster-oidc"
  }
}