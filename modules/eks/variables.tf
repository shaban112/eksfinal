variable "region" {
  type           = string
  description  = "Region for this infrastructure"
  default        = "us-east-1"
}

variable "cluster-name" {
  description = "cluster name"
  type = string
  default = "prod"
}

variable "node-group" {
  description = "node group name"
  type = string
  default = "node_group_1"
}

variable "instance-type" {
  description = "instance-type"
  type = string
  default = "t3.medium"
}


variable "path" {
  description = "path to pub key"
  type = string
  default = "~/.ssh/id_rsa.pub"
}

variable "addon_names" {
  description = "List of addon names to enable"
  type        = list(string)
  default     = ["vpc-cni", "kube-proxy", "coredns"]  # Add more addons as needed
}

variable "attach_policy_cluster" {
  description = "List of attachement policy to add to cluster"
  type        = list(string)
  default     = ["arn:aws:iam::aws:policy/AmazonEKSClusterPolicy", "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"]  
}


variable "attach_policy_nodegroup" {
  description = "List of attachement policy to add to nodegroup"
  type        = list(string)
  default     = ["arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy", "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy" , "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"]  
}


variable "oidc-thump" {
  type           = string
  description  = "tumbprint_list"
  default        = "a031c46782e6e6c662c2c87c76da9aa62ccabd8e"
}
