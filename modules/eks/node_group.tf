resource "aws_key_pair" "key" {
  key_name   = "my-key"
  public_key = file(var.path)
}

data "aws_ssm_parameter" "eks_ami_release_version" {
  name = "/aws/service/eks/optimized-ami/${aws_eks_cluster.cluster.version}/amazon-linux-2/recommended/release_version"
}
resource "aws_eks_node_group" "node_group_1" {
  cluster_name    = aws_eks_cluster.cluster.name
  node_group_name = var.node-group
  node_role_arn   = aws_iam_role.eks_role_nodegroup.arn
  subnet_ids      = module.vpc.subnet_prv[*].id
  instance_types = [var.instance-type]
  version         = aws_eks_cluster.cluster.version
  release_version = nonsensitive(data.aws_ssm_parameter.eks_ami_release_version.value)
  remote_access {
    ec2_ssh_key = aws_key_pair.key.key_name
  }

  scaling_config {
    desired_size = 2
    max_size     = 4
    min_size     = 2
  }

  update_config {
    max_unavailable = 1
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.prod_attach_policy_nodegroup,
  ]
}


data "aws_iam_policy_document" "assume_role_nodegroup" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "eks_role_nodegroup" {
  name  = "eks-nodegroup-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_nodegroup.json
}

resource "aws_iam_role_policy_attachment" "prod_attach_policy_nodegroup" {
   count = length(var.attach_policy_nodegroup)
   policy_arn = var.attach_policy_nodegroup[count.index]
   role = aws_iam_role.eks_role_nodegroup.name
 }
