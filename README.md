
Getting Started with Your EKS Cluster

This guide will walk you through the initial setup of your Amazon Elastic Kubernetes Service (EKS) cluster. You can customize the configuration to fit your specific needs.

Key Features:

Highly Available Control Plane: The cluster API is spread across three Availability Zones (AZs) for redundancy and fault tolerance.
Scalable Worker Nodes: The initial configuration includes two worker nodes, but you can easily scale this up or down as needed.
Public Cluster API: The cluster API is accessible from the internet, residing in public subnets.
Private Worker Nodes: Worker nodes are located in private subnets for enhanced security. They access the cluster API through a Network Address Translation (NAT) gateway.
Optional Public Registry Access: If your worker nodes need to pull images from public container registries, you can configure internet access through the NAT gateway.
Customization:

Feel free to modify the configuration variables to match your desired number of AZs, worker nodes, and network access settings.

Ahmed Shaban

