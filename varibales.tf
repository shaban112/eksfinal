
variable "region" {
  type           = string
  description  = "Region for this infrastructure"
  default        = "us-east-1"
}
